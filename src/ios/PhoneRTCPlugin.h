// Modified by OnSIP on 10/2/2014

#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>
#import <WebRTC/RTCEAGLVideoView.h>
#import "PhoneRTCDelegate.h"

@interface PhoneRTCPlugin : CDVPlugin<PhoneRTCProtocol>
@property(nonatomic, strong) PhoneRTCDelegate *webRTC;
@property(nonatomic, strong) NSString *sendMessageCallbackId;
@property(nonatomic, strong) RTCEAGLVideoView* localVideoView;
@property(nonatomic, strong) RTCEAGLVideoView* remoteVideoView;
@property(nonatomic, strong) RTCVideoTrack* localVideoTrack;
@property(nonatomic, strong) RTCVideoTrack* remoteVideoTrack;
@property(nonatomic, strong) RTCPeerConnectionFactory* factory;
@property(nonatomic, assign) bool callOut;


- (void)onRTCICEConnectionChanged: (CDVInvokedUrlCommand*)command;
- (void)setMicMode: (CDVInvokedUrlCommand*)command;
- (void)getDescription: (CDVInvokedUrlCommand*)command;
- (void)setDescription: (CDVInvokedUrlCommand*)command;
- (void)receiveMessage:(CDVInvokedUrlCommand*)command;
@end

@interface MessagesObserver
- (void)sendMessage:(NSString *)message;
@end
