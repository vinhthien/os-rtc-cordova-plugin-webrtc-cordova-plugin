// Modified by OnSIP 10/2/2014

#import "PhoneRTCDelegate.h"

@implementation PhoneRTCDelegate

@synthesize pcObserver;
@synthesize peerConnection;
@synthesize peerConnectionFactory;
@synthesize queuedRemoteCandidates;
@synthesize capturer;




- (id)initWithDelegate:(id)delegate andIsInitiator:(BOOL)isInitiator andICEServers:(NSArray<RTCIceServer*>*)servers
{
    self = [super init];
    
    self.delegate = delegate;
    self.isInitiator = isInitiator;
    // TODO: Remove this (might have unintended side effects)
    self.doVideo = YES;
    
    NSMutableDictionary* mandatoryConstraints = [[NSMutableDictionary alloc] init];
    [mandatoryConstraints setObject:@"true" forKey:@"OfferToReceiveAudio"];
    [mandatoryConstraints setObject:@"true" forKey:@"OfferToReceiveVideo"];
    NSMutableDictionary* optionalConstraints = [[NSMutableDictionary alloc] init];
    [optionalConstraints setObject:@"true" forKey:@"internalSctpDataChannels"];
    [optionalConstraints setObject:@"true" forKey:@"DtlsSrtpKeyAgreement"];
    self.constraints = [[RTCMediaConstraints alloc]
                        initWithMandatoryConstraints: mandatoryConstraints
                        optionalConstraints: optionalConstraints
                        ];
    
    self.queuedRemoteCandidates = [NSMutableArray array];
    self.peerConnectionFactory = [[RTCPeerConnectionFactory alloc] init];
    self.pcObserver = [[PCObserver alloc] initWithDelegate:self];
    [RTCPeerConnectionFactory initialize];
    RTCConfiguration* configuration = [[RTCConfiguration alloc] init];
    configuration.iceServers = servers;
    self.peerConnection = [self.peerConnectionFactory peerConnectionWithConfiguration:configuration constraints:[self constraints] delegate:self.pcObserver];
    
    return self;
    
}

- (void)setCallbackId:(NSString*)callbackId {
    [self.pcObserver setCallbackId:callbackId];
}

- (void)getDescription
{
    RTCMediaStream *lms = [self.peerConnectionFactory mediaStreamWithStreamId:@"ARDAMS"];
    if ([self doVideo]) {
        
        
        
        // Local capture copied from AppRTC
        NSString* cameraID = nil;
        for (AVCaptureDevice* captureDevice in
             [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo]) {
            // TODO: Make this camera option configurable
            if (captureDevice.position == AVCaptureDevicePositionFront) {
                cameraID = [captureDevice localizedName];
                break;
            }
        }
        NSAssert(cameraID, @"Unable to get the front camera id");
        
        RTCVideoSource* videoSource = [self.peerConnectionFactory videoSource];
        RTCVideoTrack* localVideoTrack = [self.peerConnectionFactory videoTrackWithSource:videoSource trackId:@"ARDAMSv0"];
        RTCCameraVideoCapturer* videoCapturer = [[RTCCameraVideoCapturer alloc] init];
        videoCapturer.delegate = videoSource;
        
        AVCaptureDevice* activeDevice = nil;
        if (@available(iOS 10.0, *)) {
            activeDevice = [AVCaptureDevice defaultDeviceWithDeviceType:AVCaptureDeviceTypeBuiltInWideAngleCamera mediaType:AVMediaTypeVideo position:AVCaptureDevicePositionFront];
        } else {
            AVCaptureDevicePosition position = AVCaptureDevicePositionFront;
            for (AVCaptureDevice* device in AVCaptureDevice.devices) {
                if (device.position == position) {
                    activeDevice = device;
                    break;
                }
            }
        }
        
        if (!activeDevice) {
            NSAssert(NO, @"Unable to get the camera");
            return;
        }
        
        NSError* error;
        AVCaptureInput* input = [AVCaptureDeviceInput deviceInputWithDevice:activeDevice error:&error];
        [videoCapturer.captureSession addInput:input];
        
        [self startCaptureWithCapturer:videoCapturer];
        
        if (localVideoTrack) {
            [lms addVideoTrack:localVideoTrack];
            [self.delegate addLocalVideoTrack:localVideoTrack];
        }
    }
    [lms addAudioTrack:[self.peerConnectionFactory audioTrackWithTrackId:@"ARDAMSa0"]];
    
    [self.peerConnection addStream:lms];
    
    // End local capture
    
    if ([self isInitiator]) {
        [self.peerConnection offerForConstraints:[self constraints] completionHandler:^(RTCSessionDescription * _Nullable sdp, NSError * _Nullable error) {
            if (error != nil) {
                return;
            }
            [self peerConnection:self.peerConnection didCreateSessionDescription:sdp error:nil];
        }];
    } else {
        [self.peerConnection answerForConstraints:[self constraints] completionHandler:^(RTCSessionDescription * _Nullable sdp, NSError * _Nullable error) {
            [self peerConnection:self.peerConnection didSetSessionDescriptionWithError:nil];
        }];
    }
}

- (void) startCaptureWithCapturer: (RTCCameraVideoCapturer*)videoCapturer {
    AVCaptureDevice* activeDevice = nil;
    if (@available(iOS 10.0, *)) {
        activeDevice = [AVCaptureDevice defaultDeviceWithDeviceType:AVCaptureDeviceTypeBuiltInWideAngleCamera mediaType:AVMediaTypeVideo position:AVCaptureDevicePositionFront];
    } else {
        AVCaptureDevicePosition position = AVCaptureDevicePositionFront;
        for (AVCaptureDevice* device in AVCaptureDevice.devices) {
            if (device.position == position) {
                activeDevice = device;
                break;
            }
        }
    }
    
    if (!activeDevice) {
        NSAssert(NO, @"Unable to get the camera");
        return;
    }
    
    
    AVCaptureDeviceFormat* format = [self selectFormatForDeviceWithCapturer:videoCapturer device:activeDevice targetWidth:640 targetHeight:480];
    
    NSInteger fps = [self selectFpsForFormat:activeDevice.activeFormat];
    [videoCapturer startCaptureWithDevice:activeDevice format:format fps:fps];
    
}

- (AVCaptureDeviceFormat*) selectFormatForDeviceWithCapturer:(RTCCameraVideoCapturer*)capturer device:(AVCaptureDevice*)device targetWidth:(int)targetWidth targetHeight:(int)targetHeight {
    
    NSArray<AVCaptureDeviceFormat*>* formats = device.formats;
    AVCaptureDeviceFormat* selectedFormat = nil;
    int currentDiff = INT_MAX;
    
    for (AVCaptureDeviceFormat* format in formats) {
        CMVideoDimensions dimension = CMVideoFormatDescriptionGetDimensions(format.formatDescription);
        FourCharCode pixelFormat = CMFormatDescriptionGetMediaSubType(format.formatDescription);
        int diff = abs(targetWidth - dimension.width) + abs(targetHeight - dimension.height);
        if (diff < currentDiff) {
            selectedFormat = format;
            currentDiff = diff;
        }
    }
    
    return selectedFormat;
}

- (NSInteger) selectFpsForFormat: (AVCaptureDeviceFormat*) format {
    NSInteger maxSupportedFramerate = 0;
    for (AVFrameRateRange* fpsRange in format.videoSupportedFrameRateRanges) {
        if (maxSupportedFramerate < fpsRange.maxFrameRate) {
            maxSupportedFramerate = fpsRange.maxFrameRate;
        }
    }
    if (maxSupportedFramerate > 30) {
        return 30;
    }
    return maxSupportedFramerate;
}

- (void)mute
{
    if(self.peerConnection.localStreams.count > 0) {
        RTCMediaStream *lStream = self.peerConnection.localStreams[0];
        if(lStream.audioTracks.count > 0) { // Usually we will have only one track. If you have more than one, need to traverse all.
            RTCAudioTrack* audioTrack = lStream.audioTracks[0];
            if (audioTrack) {
                [audioTrack setIsEnabled:NO];
            }
        }
    }
}

- (void)unmute
{
    if(self.peerConnection.localStreams.count > 0) {
        RTCMediaStream *lStream = self.peerConnection.localStreams[0];
        if(lStream.audioTracks.count > 0) { // Usually we will have only one track. If you have more than one, need to traverse all.
            RTCAudioTrack* audioTrack = lStream.audioTracks[0];
            if (audioTrack) {
                [audioTrack setIsEnabled:YES];
            }
        }
    }
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection
didCreateSessionDescription:(RTCSessionDescription *)origSdp
                 error:(NSError *)error {
    if (error) {
        NSLog(@"SDP OnFailure: %@", error);
        NSAssert(NO, error.description);
        return;
    }
    
    RTCSessionDescription* sdp =
    [[RTCSessionDescription alloc]
     initWithType:origSdp.type
     sdp:[PhoneRTCDelegate preferISAC:origSdp.sdp]];
    [self.peerConnection setLocalDescription:sdp completionHandler:^(NSError * _Nullable error) {
        if (error) {
            return;
        }
        [self peerConnection:self.peerConnection didSetSessionDescriptionWithError:nil];
    }];
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        NSString* sdpType = @"offer";
        if (sdp.type == RTCSdpTypeOffer) {
            sdpType = @"offer";
        } else if (sdp.type == RTCSdpTypePrAnswer) {
            sdpType = @"phonertc_handshake";
        } else if (sdp.type == RTCSdpTypeAnswer) {
            sdpType = @"answer";
        }
   
        NSDictionary *json = @{ @"type" : sdpType, @"sdp" : sdp.sdp };
        NSError *error2;
        NSData *data =
            [NSJSONSerialization dataWithJSONObject:json options:0 error:&error2];
        NSAssert(!error,
                 @"%@",
                 [NSString stringWithFormat:@"Error: %@", error2.description]);
        [self sendMessage:data];
    });
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection
didSetSessionDescriptionWithError:(NSError *)error {
    if (error) {
        //        NSAssert(NO, error.description);
        //TODO: handle some thing here
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        if ([self isInitiator]) {
            if (self.peerConnection.remoteDescription) {
                NSLog(@"SDP onSuccess - drain candidates");
                [self drainRemoteCandidates];
            }
        }
        else {
            if (self.peerConnection.localDescription != nil) {
                [self drainRemoteCandidates];
            }
        }
    });
}

+ (NSString *)firstMatch:(NSRegularExpression *)pattern
              withString:(NSString *)string {
    NSTextCheckingResult* result =
    [pattern firstMatchInString:string
                        options:0
                          range:NSMakeRange(0, [string length])];
    if (!result)
        return nil;
    return [string substringWithRange:[result rangeAtIndex:1]];
}

+ (NSString *)preferISAC:(NSString *)origSDP {
    int mLineIndex = -1;
    NSString* isac16kRtpMap = nil;
    origSDP = [origSDP stringByReplacingOccurrencesOfString:@"\r\n" withString:@"\n"];
    NSArray* lines = [origSDP componentsSeparatedByString:@"\n"];
    NSRegularExpression* isac16kRegex = [NSRegularExpression
                                         regularExpressionWithPattern:@"^a=rtpmap:(\\d+) ISAC/16000[\r]?$"
                                         options:0
                                         error:nil];
    for (int i = 0;
         (i < [lines count]) && (mLineIndex == -1 || isac16kRtpMap == nil);
         ++i) {
        NSString* line = [lines objectAtIndex:i];
        if ([line hasPrefix:@"m=audio "]) {
            mLineIndex = i;
            continue;
        }
        isac16kRtpMap = [self firstMatch:isac16kRegex withString:line];
    }
    if (mLineIndex == -1) {
        NSLog(@"No m=audio line, so can't prefer iSAC");
        return origSDP;
    }
    if (isac16kRtpMap == nil) {
        NSLog(@"No ISAC/16000 line, so can't prefer iSAC");
        return origSDP;
    }
    NSArray* origMLineParts =
    [[lines objectAtIndex:mLineIndex] componentsSeparatedByString:@" "];
    NSMutableArray* newMLine =
    [NSMutableArray arrayWithCapacity:[origMLineParts count]];
    int origPartIndex = 0;
    // Format is: m=<media> <port> <proto> <fmt> ...
    [newMLine addObject:[origMLineParts objectAtIndex:origPartIndex++]];
    [newMLine addObject:[origMLineParts objectAtIndex:origPartIndex++]];
    [newMLine addObject:[origMLineParts objectAtIndex:origPartIndex++]];
    [newMLine addObject:isac16kRtpMap];
    for (; origPartIndex < [origMLineParts count]; ++origPartIndex) {
        if ([isac16kRtpMap compare:[origMLineParts objectAtIndex:origPartIndex]]
            != NSOrderedSame) {
            [newMLine addObject:[origMLineParts objectAtIndex:origPartIndex]];
        }
    }
    NSMutableArray* newLines = [NSMutableArray arrayWithCapacity:[lines count]];
    [newLines addObjectsFromArray:lines];
    [newLines replaceObjectAtIndex:mLineIndex
                        withObject:[newMLine componentsJoinedByString:@" "]];
    return [newLines componentsJoinedByString:@"\r\n"];
}

- (void)disconnect {
    [self.delegate resetUi];
    [self sendMessage:[@"{\"type\": \"bye\"}" dataUsingEncoding:NSUTF8StringEncoding]];
    [self.peerConnection close];
    self.peerConnection = nil;
    self.pcObserver = nil;
    self.constraints = nil;
//    [RTCPeerConnectionFactory deinitializeSSL];
//    self.peerConnectionFactory = nil;
    self.capturer = nil;
    
    [self sendMessage:[@"{\"type\": \"__disconnected\"}" dataUsingEncoding:NSUTF8StringEncoding]];
}

- (void)drainRemoteCandidates {
    for (RTCIceCandidate *candidate in self.queuedRemoteCandidates) {
        [self.peerConnection addIceCandidate:candidate];
    }
    self.queuedRemoteCandidates = nil;
}

- (void)sendMessage:(NSData *)message
{
    NSLog(@"sendMessage 1");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SendMessage" object:message];
}

- (void)sendRemoteVideoTrack:(RTCVideoTrack* )track
{
    [self.delegate addRemoteVideoTrack:track];
}

- (void)receiveMessage:(NSString *)message
{
    NSError *error;
    NSDictionary *objects = [NSJSONSerialization
                             JSONObjectWithData:[message dataUsingEncoding:NSUTF8StringEncoding]
                             options:0
                             error:&error];
    // NSAssert(!error,
    //          @"%@",
    //          [NSString stringWithFormat:@"Error: %@", error.description]);
    // NSAssert([objects count] > 0, @"Invalid JSON object");
    
    NSString *value = [objects objectForKey:@"type"];
    if ([value compare:@"candidate"] == NSOrderedSame) {
        NSString *mid = [objects objectForKey:@"id"];
        NSNumber *sdpLineIndex = [objects objectForKey:@"label"];
        NSString *sdp = [objects objectForKey:@"candidate"];
        
        RTCIceCandidate *candidate = [[RTCIceCandidate alloc] initWithSdp:sdp sdpMLineIndex:sdpLineIndex.intValue sdpMid:mid];
        if (self.queuedRemoteCandidates) {
            [self.queuedRemoteCandidates addObject:candidate];
        } else {
            [self.peerConnection addIceCandidate:candidate];
        }
    } else if ([value compare:@"offer"] == NSOrderedSame) {
        NSString *sdpString = [objects objectForKey:@"sdp"];
        [self receiveOffer:sdpString];
    } else if ([value compare:@"answer"] == NSOrderedSame) {
        NSString *sdpString = [objects objectForKey:@"sdp"];
        [self receiveAnswer:sdpString];
    } else if ([value compare:@"bye"] == NSOrderedSame) {
        [self disconnect];
    } else {
        
        //        NSAssert(NO, @"Invalid message: %@", message);
    }
}

- (void)setRemoteDescription:(NSString*)description withType:(NSString *)typeString
{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        RTCSdpType type = RTCSdpTypeOffer;
        if ([typeString isEqualToString:@"offer"]) {
            type = RTCSdpTypeOffer;
        } else if ([typeString isEqualToString:@"answer"]) {
            type = RTCSdpTypeAnswer;
        } else if ([typeString isEqualToString:@"phonertc_handshake"]) {
            type = RTCSdpTypePrAnswer;
        }
        
        RTCSessionDescription *sdp = [[RTCSessionDescription alloc]
                                      initWithType:type sdp:[PhoneRTCDelegate preferISAC:description]];
        [self.peerConnection setRemoteDescription:sdp completionHandler:^(NSError * _Nullable error) {
            if (error) {
                return;
            }
            if (type == RTCSdpTypeOffer) {
                [self peerConnection:self.peerConnection didSetSessionDescriptionWithError:nil];
            } else if (type == RTCSdpTypeAnswer) {
                [self peerConnection:self.peerConnection didSetSessionDescriptionWithError:nil];
            }
        }];
    });
}

- (void)receiveAnswer:(NSString *)sdpString
{
    [self setRemoteDescription:sdpString withType:@"answer"];
}

- (void)receiveOffer:(NSString *)sdpString
{
    [self setRemoteDescription:sdpString withType:@"offer"];
}

@end


@implementation PCObserver {
    id<PHONERTCSendMessage> _delegate;
}

- (id)initWithDelegate:(id<PHONERTCSendMessage>)delegate {
    if (self = [super init]) {
        _delegate = delegate;
    }
    return self;
}

- (void)setCallbackId:(NSString*)callbackId {
    self._callbackId = [[NSString alloc] initWithString:callbackId];
}

- (void)peerConnectionOnError:(RTCPeerConnection *)peerConnection {
    NSLog(@"PCO onError.");
    NSAssert(NO, @"PeerConnection failed.");
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection
 didChangeSignalingState:(RTCSignalingState)stateChanged {
    NSLog(@"PCO onSignalingStateChange: %d", stateChanged);
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection
           didAddStream:(RTCMediaStream *)stream {
    NSLog(@"PCO onAddStream.");
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        NSAssert([stream.audioTracks count] >= 1,
                 @"Expected at least 1 audio stream");
        if ([stream.videoTracks count] > 0) {
            [_delegate sendRemoteVideoTrack:stream.videoTracks[0]];
        }
    });
[_delegate sendMessage:[@"{\"type\": \"__answered\"}" dataUsingEncoding:NSUTF8StringEncoding]];
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection
         removedStream:(RTCMediaStream *)stream {
    NSLog(@"PCO onRemoveStream.");
    [_delegate resetUi];
}

- (void)
peerConnectionOnRenegotiationNeeded:(RTCPeerConnection *)peerConnection {
    NSLog(@"PCO onRenegotiationNeeded.");
    // TODO(hughv): Handle this.
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection
       didGenerateIceCandidate:(RTCIceCandidate *)candidate {
    NSLog(@"PCO onICECandidate.\n  Mid[%@] Index[%d] Sdp[%@]",
          candidate.sdpMid,
          candidate.sdpMLineIndex,
          candidate.sdp);
    NSDictionary *json =
    @{ @"type" : @"candidate",
       @"label" : [NSNumber numberWithInt:candidate.sdpMLineIndex],
       @"id" : candidate.sdpMid,
       @"candidate" : candidate.sdp };
    NSError *error;
    NSData *data = [NSJSONSerialization dataWithJSONObject:json options:0 error:&error];
    if (!error) {
        NSLog(@"gotICECandidate -- sending message");
        [_delegate sendMessage:data];
    } else {
        NSAssert(NO, @"Unable to serialize JSON object with error: %@",
                 error.localizedDescription);
    }
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection
   didChangeIceGatheringState:(RTCIceGatheringState)newState {
    NSLog(@"PCO onIceGatheringChange. %d", newState);
    NSString *stateString;
    switch(newState) {
        case RTCIceGatheringStateNew:
            stateString = @"NEW";
            break;
        case RTCIceGatheringStateGathering:
            stateString = @"GATHERING";
            break;
        case RTCIceGatheringStateComplete:
            stateString = @"COMPLETE";
            break;
    }
    NSDictionary *json =
    @{ @"type" : @"IceGatheringChange",
       @"state" : stateString
       };
    NSError *error;
    [_delegate sendMessage:[NSJSONSerialization dataWithJSONObject:json options:0 error:&error]];
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection
  didChangeIceConnectionState:(RTCIceConnectionState)newState {
    NSLog(@"PCO onIceConnectionChange. %d", newState);
    
    NSString* stateString = [[NSString alloc] initWithFormat:@"%d", newState];
    if (self._callbackId) {
        NSDictionary *userInfo = @{@"RTCIceConnectionState":stateString,@"callbackId": self._callbackId};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"onRTCICEConnectionChanged" object:nil userInfo:userInfo];
    }
    //    NSAssert(newState != RTCICEConnectionFailed, @"ICE Connection failed!");
}

/** Called when negotiation is needed, for example ICE has restarted. */
- (void)peerConnectionShouldNegotiate:(RTCPeerConnection *)peerConnection {
    // nothing
}


@end
