// Modified by OnSIP on 10/2/2014

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <Cordova/CDVViewController.h>

#import <WebRTC/RTCIceCandidate.h>
#import <WebRTC/RTCICEServer.h>
#import <WebRTC/RTCMediaConstraints.h>
#import <WebRTC/RTCMediaStream.h>
#import <WebRTC/RTCPeerConnection.h>
#import <WebRTC/RTCPeerConnectionFactory.h>
#import <WebRTC/RTCSessionDescription.h>
#import <WebRTC/RTCVideoRenderer.h>
#import <WebRTC/RTCVideoCapturer.h>
#import <WebRTC/RTCVideoTrack.h>
#import <WebRTC/RTCConfiguration.h>
#import <WebRTC/RTCCameraVideoCapturer.h>
#import <WebRTC/RTCAudioTrack.h>


#import "RTCSessionDescriptionDelegate.h"

@protocol PHONERTCSendMessage<NSObject>
- (void)sendMessage:(NSData*)message;
- (void)sendRemoteVideoTrack:(RTCVideoTrack*)track;
- (void)resetUi;
@end

@protocol PhoneRTCProtocol<NSObject>
- (void)addLocalVideoTrack:(RTCVideoTrack *)track;
- (void)addRemoteVideoTrack:(RTCVideoTrack *)track;
- (void)resetUi;
@end

@interface PCObserver : NSObject<RTCPeerConnectionDelegate>
- (id)initWithDelegate:(id<PHONERTCSendMessage>)delegate;
- (void)setCallbackId:(NSString*)callbackId;

@property(nonatomic, strong) NSString* _callbackId;
@end

@protocol ICEServerDelegate<NSObject>
- (void)getDescription;
@end

@interface PhoneRTCDelegate : UIResponder<ICEServerDelegate,
PHONERTCSendMessage,
RTCSessionDescriptionDelegate>
@property(nonatomic, strong) PCObserver *pcObserver;
@property(nonatomic, strong) RTCPeerConnection *peerConnection;
@property(nonatomic, strong) RTCPeerConnectionFactory *peerConnectionFactory;
@property(nonatomic, strong) NSMutableArray *queuedRemoteCandidates;
@property(nonatomic, strong) RTCMediaConstraints *constraints;
@property(nonatomic, weak) id<PhoneRTCProtocol> delegate;
@property(nonatomic, strong) RTCVideoCapturer *capturer;
@property(assign) BOOL doVideo;
@property(assign) BOOL isInitiator;

- (id)initWithDelegate:(id)delegate andIsInitiator:(BOOL)isInitiator andICEServers:(NSArray*)servers;
- (void)setCallbackId:(NSString*)callbackId;
- (void)receiveMessage:(NSString*)message;
- (void)receiveOffer:(NSString *)message;
- (void)receiveAnswer:(NSString *)message;
- (void)mute;
- (void)unmute;
- (void)disconnect;
@end
