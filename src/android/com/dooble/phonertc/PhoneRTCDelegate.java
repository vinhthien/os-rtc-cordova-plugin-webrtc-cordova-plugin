package com.dooble.phonertc;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.AudioSource;
import org.webrtc.DataChannel;
import org.webrtc.IceCandidate;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;
import org.webrtc.VideoCapturerAndroid;
import org.webrtc.VideoSource;
import org.webrtc.VideoTrack;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by vinhthien on 3/9/17.
 */

public class PhoneRTCDelegate {

    public boolean doVideo;
    public boolean isInitiator;

    private PhoneRTCPlugin delegate;
    private MediaConstraints constraints;
    private List<IceCandidate> queuedRemoteCandidates;
    private PeerConnectionFactory peerConnectionFactory;
    private PCObserver pcObserver = new PCObserver();
    private SDPObserver sdpObserver = new SDPObserver();
    private PeerConnection peerConnection;
    private VideoCapturerAndroid capturer;
    private MediaStream localStream;

    public PhoneRTCDelegate(PhoneRTCPlugin delegate, List<PeerConnection.IceServer> iceServerList, boolean isInitiator) {
        this.isInitiator = isInitiator;
        this.delegate = delegate;
        this.doVideo = true;

        this.constraints = new MediaConstraints();
        this.constraints.mandatory.add(new MediaConstraints.KeyValuePair(
                "OfferToReceiveAudio", "true"));
        this.constraints.mandatory.add(new MediaConstraints.KeyValuePair(
                "OfferToReceiveVideo", "true"));
        this.constraints.optional.add(new MediaConstraints.KeyValuePair(
                "internalSctpDataChannels", "true"));
        this.constraints.optional.add(new MediaConstraints.KeyValuePair(
                "DtlsSrtpKeyAgreement", "true"));

        queuedRemoteCandidates = new ArrayList();



        PeerConnectionFactory.initializeAndroidGlobals(delegate.getActivity(), true, true, true);
        peerConnectionFactory = new PeerConnectionFactory();


        //TODO: [RTCPeerConnectionFactory initializeSSL];

        peerConnection = peerConnectionFactory.createPeerConnection(iceServerList,constraints,pcObserver);

    }



    public void getDescription() {
        this.localStream = peerConnectionFactory.createLocalMediaStream("ARDAMS");
        if (doVideo) {
            capturer = delegate.getVideoCapturer();

            VideoSource videoSource = peerConnectionFactory.createVideoSource(capturer, constraints);
            VideoTrack localVideoTrack = peerConnectionFactory.createVideoTrack("ARDAMSv0", videoSource);
            if (localVideoTrack != null) {
                localStream.addTrack(localVideoTrack);
                delegate.addLocalVideoTrack(localVideoTrack);
            }
        }


        AudioSource audioSource = peerConnectionFactory.createAudioSource(constraints);
        localStream.addTrack(peerConnectionFactory.createAudioTrack("ARDAMSa0", audioSource));
        peerConnection.addStream(localStream);
        if (isInitiator) {
            peerConnection.createOffer(sdpObserver, constraints);
        } else {
            peerConnection.createAnswer(sdpObserver, constraints);
        }

    }

    private void setRemoteDescription(final String description, final String type) {
        delegate.getActivity().runOnUiThread(new Runnable() {
            public void run() {
                SessionDescription sdp = new SessionDescription(
                        SessionDescription.Type.fromCanonicalForm(type),
                        preferISAC(description));
                peerConnection.setRemoteDescription(sdpObserver, sdp);
            }
        });
    }

    String preferISAC(String sdpDescription) {
        String[] lines = sdpDescription.split("\r?\n");
        int mLineIndex = -1;
        String isac16kRtpMap = null;
        Pattern isac16kPattern = Pattern
                .compile("^a=rtpmap:(\\d+) ISAC/16000[\r]?$");
        for (int i = 0; (i < lines.length)
                && (mLineIndex == -1 || isac16kRtpMap == null); ++i) {
            if (lines[i].startsWith("m=audio ")) {
                mLineIndex = i;
                continue;
            }
            Matcher isac16kMatcher = isac16kPattern.matcher(lines[i]);
            if (isac16kMatcher.matches()) {
                isac16kRtpMap = isac16kMatcher.group(1);
                continue;
            }
        }
        if (mLineIndex == -1) {
            Log.d("com.dooble.phonertc",
                    "No m=audio line, so can't prefer iSAC");
            return sdpDescription;
        }
        if (isac16kRtpMap == null) {
            Log.d("com.dooble.phonertc",
                    "No ISAC/16000 line, so can't prefer iSAC");
            return sdpDescription;
        }
        String[] origMLineParts = lines[mLineIndex].split(" ");
        StringBuilder newMLine = new StringBuilder();
        int origPartIndex = 0;
        // Format is: m=<media> <port> <proto> <fmt> ...
        newMLine.append(origMLineParts[origPartIndex++]).append(" ");
        newMLine.append(origMLineParts[origPartIndex++]).append(" ");
        newMLine.append(origMLineParts[origPartIndex++]).append(" ");
        newMLine.append(isac16kRtpMap).append(" ");
        for (; origPartIndex < origMLineParts.length; ++origPartIndex) {
            if (!origMLineParts[origPartIndex].equals(isac16kRtpMap)) {
                newMLine.append(origMLineParts[origPartIndex]).append(" ");
            }
        }
        lines[mLineIndex] = newMLine.toString();
        StringBuilder newSdpDescription = new StringBuilder();
        for (String line : lines) {
            newSdpDescription.append(line).append("\r\n");
        }
        return newSdpDescription.toString();
    }

    public void receiveOffer(String sdp) {
        setRemoteDescription(sdp, "offer");
    }

    public void receiveAnswer(String sdp) {
        setRemoteDescription(sdp, "answer");
    }

    public void mute() {
        if (this.localStream == null) {
            return;
        }
        if (this.localStream != null
                && this.localStream.audioTracks != null && this.localStream.audioTracks.size() > 0) {
            this.localStream.audioTracks.get(0).setEnabled(false);
        }
    }

    public void unmute() {
        if (this.localStream == null) {
            return;
        }
        if (this.localStream != null
                && this.localStream.audioTracks != null && this.localStream.audioTracks.size() > 0) {
            this.localStream.audioTracks.get(0).setEnabled(true);
        }
    }

    public void receiveMessage(String message) {
        try {
            JSONObject objects = new JSONObject(message);
            String value = objects.getString("type");
            if (value.equals("candidate")) {
                String mid = objects.getString("");
                int sdpLineIndex = objects.getInt("label");
                String sdp = objects.getString("candidate");
                IceCandidate candidate = new IceCandidate(mid, sdpLineIndex, sdp);
                if (queuedRemoteCandidates != null) {
                    queuedRemoteCandidates.add(candidate);
                } else {
                    peerConnection.addIceCandidate(candidate);
                }
            } else if (value.equals("offer")) {
                String sdpString = objects.getString("sdp");
                receiveOffer(sdpString);
            } else if (value.equals("answer")) {
                String sdpString = objects.getString("sdp");
                receiveAnswer(sdpString);
            } else if (value.equals("bye")) {
                disconnect();
            } else {
                // nothing
            }
        } catch (JSONException e) {
            // nothing
        }
    }

    public void disconnect() {
        delegate.resetUi();
        sendMessage("{\"type\": \"bye\"}");

        peerConnection.close();
        peerConnection = null;
        pcObserver = null;
        constraints = null;
//       TODO [RTCPeerConnectionFactory deinitializeSSL];
        peerConnectionFactory = null;
        capturer = null;

        sendMessage("{\\\"type\\\": \\\"__disconnected\\\"}");
    }

    public void sendMessage(String message)
    {
        delegate.sendMessage(message);
    }

    private class PCObserver implements PeerConnection.Observer {

        @Override
        public void onIceCandidate(final IceCandidate iceCandidate) {
            delegate.getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    try {
                        JSONObject json = new JSONObject();
                        json.put("type", "candidate");
                        json.put("label", iceCandidate.sdpMLineIndex);
                        json.put("id", iceCandidate.sdpMid);
                        json.put("candidate", iceCandidate.sdp);
                        sendMessage(json.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        public void onAddStream(final MediaStream stream) {
            delegate.getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    if (stream.videoTracks.size() > 0) {
                        delegate.addRemoteVideoTrack(stream.videoTracks.get(0));
                    }

                    try {
                        JSONObject data = new JSONObject();
                        data.put("type", "__answered");
                        sendMessage(data.toString());
                    } catch (JSONException e) {

                    }
                }
            });
        }

        @Override
        public void onDataChannel(DataChannel stream) {
            // TODO Auto-generated method stub

        }



        @Override
        public void onIceConnectionChange(PeerConnection.IceConnectionState arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onIceGatheringChange(PeerConnection.IceGatheringState arg0) {
            try {
                JSONObject json = new JSONObject();
                json.put("type", "IceGatheringChange");
                json.put("state", arg0.name());
                sendMessage(json.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onRemoveStream(MediaStream stream) {
            delegate.resetUi();
        }

        @Override
        public void onRenegotiationNeeded() {
            // TODO Auto-generated method stub

        }

        @Override
        public void onSignalingChange(
                PeerConnection.SignalingState signalingState) {

        }

        @Override
        public void onIceCandidatesRemoved(IceCandidate[] candidates) {

        }

        @Override
        public void onIceConnectionReceivingChange(boolean receiving) {

        }

    }

    private class SDPObserver implements SdpObserver {
        @Override
        public void onCreateSuccess(final SessionDescription origSdp) {
            delegate.getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    SessionDescription sdp = new SessionDescription(
                            origSdp.type, preferISAC(origSdp.description));
                    try {
                        JSONObject json = new JSONObject();
                        json.put("type", sdp.type.canonicalForm());
                        json.put("sdp", sdp.description);
                        sendMessage(json.toString());
                        peerConnection.setLocalDescription(sdpObserver, sdp);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        public void onSetSuccess() {
            delegate.getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    if (isInitiator) {
                        if (peerConnection.getRemoteDescription() != null) {
                            // We've set our local offer and received & set the
                            // remote
                            // answer, so drain candidates.
                            drainRemoteCandidates();
                        }
                    } else {
                        if (peerConnection.getLocalDescription() == null) {
                            // We just set the remote offer, time to create our
                            // answer.
                            peerConnection.createAnswer(SDPObserver.this,
                                    constraints);
                        } else {
                            // Sent our answer and set it as local description;
                            // drain
                            // candidates.
                            drainRemoteCandidates();
                        }
                    }
                }
            });
        }

        @Override
        public void onCreateFailure(final String error) {
            delegate.getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    throw new RuntimeException("createSDP error: " + error);
                }
            });
        }

        @Override
        public void onSetFailure(final String error) {
            delegate.getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    //throw new RuntimeException("setSDP error: " + error);
                }
            });
        }

        private void drainRemoteCandidates() {
                if (queuedRemoteCandidates == null)
                    return;

                for (IceCandidate candidate : queuedRemoteCandidates) {
                    peerConnection.addIceCandidate(candidate);
                }

                queuedRemoteCandidates = null;
        }
    }

}
